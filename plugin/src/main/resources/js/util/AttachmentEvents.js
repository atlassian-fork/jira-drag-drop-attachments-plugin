/**
 * A dictionary of attachment related event types that JIRA will fire from time to time.
 * @module jira/util/events/types
 */
define('dndattachment/util/events/types', {
    // Export the event name so listeners don't have to
    ATTACHMENT_FOR_PAGE_RECEIVED: "attachmentForPageReceived"
});
