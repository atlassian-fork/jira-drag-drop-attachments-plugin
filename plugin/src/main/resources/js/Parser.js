define('dndattachment/Parser', ['require'], function(require) {
    var Events = require('jira/util/events');
    var Types = require('jira/util/events/types');
    var Deferred = require('jira/jquery/deferred');
    var $ = require('jquery');
    var PARSED_MARKCLASS = '-dui-type-parsed';
    var contextSelector = '*[duiType]:not(.'+PARSED_MARKCLASS+')';

    function parse(context) {
        var queue = $(context).find(contextSelector).add(context.filter(contextSelector));

        var deferreds = queue.addClass(PARSED_MARKCLASS).map(function(idx, el) {
            var result = new Deferred();

            require([$(el).attr('duiType')], function(duiType) {
                if(typeof duiType == "function")
                    result.resolve(new duiType(el));
                else
                    result.reject();
            });

            return result;
        });

        return $.when.apply(window, $.makeArray(deferreds));
    }

    Events.bind(Types.NEW_CONTENT_ADDED, function(e, context, reason) {
        parse(context);
    });

    if($.isReady) {
        parse($('body'));
    } else {
        $(function() {
            parse($('body'));
        });
    }

    return {
        parse: parse
    };
});
