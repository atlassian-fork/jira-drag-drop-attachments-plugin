/**
 * An executor is what the handler calls to actually execute the upload
 * @typedef {Object} Executor
 *  @function {boolean} isValid(event, args) - returns true if the executor wants to be invoked.
 *  @function {deferred} processFiles(files, attachmentDropZone) - handles the actual processing of the file.
 *  Should return a deferred that properly communicates the outcome
 *  @property {number} weight - the weight of the executor when we loop through them. Higher the number, the earier we check it
 *  @property {string} name - name of the executor. This is used as the 'key' when we register and remove executors.
 */

/**
 * This listens for EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED and handles the uploading of the files.
 * This does not do the actual uploading, just collects the events and pass them along.
 */
define('dndattachment/upload/handler', ['require', 'exports'], function(require, exports) {
    var analytics = require('jira/analytics');
    var Dialog = require('jira/dialog/dialog');
    var Deferred = require('jira/jquery/deferred');
    var Events = require('jira/util/events');
    var EventTypes = require('dndattachment/util/events/types');
    var _ = require('underscore');
    var $ = require('jquery');
    var Utility = require('dndattachment/ctrlv/utility');

    // The list of registered upload executors
    var executorList = [];

    // The current attachmentDropZone that is relevant to us
    var attachmentDropZone;

    /**
     * This method is called to handle the ATTACHMENT_FOR_PAGE_RECEIVED event once it has been triggered by something.
     *
     * @param event - This will be ATTACHMENT_FOR_PAGE_RECEIVED
     * @param args - These are all the arguments that are passed in when the event is thrown. Typically they will
     * contain files, successCallback, failureCallback and alwaysCallback but may contain more things depending on the code
     * that receives the file.
     */
    var handleAttachmentReceived = function(event, args) {
        var uploadResult = Deferred();

        //if there's a dialog open, we simulate a drop and let the dialog handle it.
        var JIRADialog = Dialog.current;
        if (JIRADialog && JIRADialog.$form) {
            analytics.send({ name : 'attach.screenshot.html5.dialogPaste', data : {}});

            var fileBlob = Utility.createBlobFromFile(args.files[0]);
            if(Utility.dropFileToElement(fileBlob, JIRADialog.$form)) {
                uploadResult.resolve([args.files[0].name]);
            }
        }
        else {
            var validExecutor = _.find(executorList, function(executor) {
                return executor.isValid(event, args);
            });
            if(validExecutor) {
                uploadResult = validExecutor.processFiles(args.files, attachmentDropZone);
                if(args.successCallback) {
                    uploadResult.done(args.successCallback);
                }
                if(args.failureCallback) {
                    uploadResult.fail(args.failureCallback);
                }
                if(args.alwaysCallback) {
                    uploadResult.always(args.alwaysCallback);
                }
            }
        }

        uploadResult.done(function(fileNames, noInsertMarkup) {
            if (!noInsertMarkup && args.isWikiTextfieldFocused) {
                var wikiTextfield = args.wikiTextfield;
                _.each(fileNames, function(fileName) {
                    Utility.insertWikiMarkup(fileName, wikiTextfield, wikiTextfield.selectionStart, wikiTextfield.selectionStart);
                });
                if (JIRADialog && args.isPaste) {
                    analytics.send({ name : 'attach.screenshot.html5.dialogPaste.insertWikiMarkup', data : {}});
                }
                // Focus back on the textfield if the executor caused it to lose focus.
                if (!$(args.wikiTextfield).is(":focus")) {
                    setTimeout(function () {
                        args.wikiTextfield.focus();
                    }, 0);
                }
            }
        });
    };

    var isExecutorValid = function(executor) {
        // Check that the executor exists
        if(executor) {
            // Check that the valid methods are defined
            var hasIsValidMethod = typeof executor.isValid !== 'undefined';
            var hasProcessFiles = typeof executor.processFiles !== 'undefined';
            var hasWeight = typeof executor.weight !== 'undefined';
            var hasName = typeof executor.name !== 'undefined';

            return hasIsValidMethod && hasProcessFiles && hasWeight && hasName;
        }
        return false;
    };

    /**
     * This registers an executor for handling the uploads.
     *
     * @param {Executor} executor - The executor to register
     * @returns {Boolean} - true if the registration was successful
     */
    exports.registerExecutor = function(executor) {
        var isValid = isExecutorValid(executor);
        if(isValid) {
            // Prevent duplicate executors
            var list = _.reject(executorList, function (ex) {
                return ex.name === executor.name;
            });

            // Add in the executor
            list.push(executor);

            // Sort by weight
            executorList = _.sortBy(list, function (ex) {
                // Negative weight so that highest numbers are sorted first
                return -ex.weight;
            });
        }
        return isValid;
    };

    /**
     * This unregisters an executor
     *
     * @param {Executor} executor - The executor to register
     */
    exports.unregisterExecutor = function(executor) {
        executorList = _.reject(executorList, function(ex) {
            return ex.name === executor.name;
        });
    };

    /**
     * Initialised the upload handler
     */
    exports.initialize = function() {
        // Handle any attachments that need to be uploaded directly onto the page through the main attachment zone
        Events.bind(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, handleAttachmentReceived);
    };

    /**
     * Remove the upload handler
     */
    exports.disable = function() {
        // Cleanup everything that was done in the initialize method
        Events.unbind(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, handleAttachmentReceived);
    };

    /**
     * Set the attachmentDropZone that should handle incoming attachments
     */
    exports.setAttachmentDropZone = function(dropZone) {
        attachmentDropZone = dropZone;
    };
});
