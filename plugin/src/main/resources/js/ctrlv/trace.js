define("dndattachment/ctrlv/trace", ['require'], function (require) {
  "use strict";
  var logger = require('jira/util/logger');
  return logger.trace;
});
