define("dndattachment/ctrlv/path", ['exports'], function (exports) {
    "use strict";

    /**
     * Get the basename of a URI (i.e. the last path component).
     * @param {String} uri
     * @returns {String}
     */
    var basename = function basename(uri) {
        return uri.split(/\//).pop();
    };

    exports.basename = basename;

    /**
     * Get everything *except* the basename of a URI.
     * @param {String} uri
     * @returns {String}
     */
    exports.dirname = function (uri) {
        var basenameResult = basename(uri);
        return uri.substring(0, uri.length - basenameResult.length);
    };
});
