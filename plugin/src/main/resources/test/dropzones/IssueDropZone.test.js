AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:dnd-issue-drop-zone"
], function(){
    "use strict";

    var $ = require('jquery');
    var IssueDropZone = require('dndattachment/dropzones/IssueDropZone');
    var UploadProgressBar = require('dndattachment/progressbars/UploadProgressBar');

    module("TestIssueDropZone", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            JIRA.Events.LOCK_PANEL_REFRESHING = "lockPanelRefreshing";
            JIRA.Events.REFRESH_ISSUE_PAGE = "refreshIssuePage";
        },

        teardown: function() {
            this.sandbox.restore();
            delete JIRA.Events.LOCK_PANEL_REFRESHING;
            delete JIRA.Events.REFRESH_ISSUE_PAGE;
        }
    });

    test("IssueDropZone calls render", function() {
        var $fixture = $('#qunit-fixture');
        var node$ = $('<div></div>').appendTo($fixture);
        this.sandbox.stub(JIRA, "trigger");
        this.sandbox.stub(JIRA.SmartAjax, "makeRequest").returns(new $.Deferred());
        this.sandbox.spy(IssueDropZone.prototype, "render");

        var instance = new IssueDropZone($fixture);

        ok(IssueDropZone.prototype.render.calledOnce, "render called");
        ok(JIRA.SmartAjax.makeRequest.callCount == 0, "makeRequest not called");
    });

    test("IssueDropZone uploadFiles test", function(assert) {
        var done = assert.async();
        var $fixture = $('#qunit-fixture');
        var fileUpload = new $.Deferred();

        this.sandbox.stub(UploadProgressBar.prototype, "init", UploadProgressBar.prototype.init);
        this.sandbox.stub(UploadProgressBar.prototype, "uploadFile", function() {
            this.setFileID(1);
            return fileUpload;
        });

        var instance = new (Class.extend({
            progressBarType: 'dndattachment/progressbars/UploadProgressBar',
            $node: $fixture,
            commitUpload: this.sandbox.stub().returns(new $.Deferred().resolve()),
            uploadLimit: Number.MAX_VALUE,
            queueTask: this.sandbox.stub(),
            createUploadProgressBar: IssueDropZone.prototype.createUploadProgressBar,
            placeUploadProgressBar: IssueDropZone.prototype.placeUploadProgressBar,
            configureUploadProgressBar: IssueDropZone.prototype.configureUploadProgressBar,
            handleNewProgressBar: IssueDropZone.prototype.handleNewProgressBar,
            queueEvent: function() { }
        }))();

        var files = [{ }, { }];
        var tempFiles = [{ id: 1, name: 'dupa' }];
        var fileIDs = [1, 1];

        IssueDropZone.prototype.uploadFiles.apply(instance, [files]).done(function() {
            ok(UploadProgressBar.prototype.init.calledTwice, 'progress bar initialized twice');
            ok(UploadProgressBar.prototype.uploadFile.calledTwice, 'uploadFiles called twice');
            ok(instance.commitUpload.calledOnce, 'commitUpload called once');
            ok(instance.commitUpload.getCall(0).args[0].join() == fileIDs.join(), 'commitUpload called with fileIDs');
            done();
        });

        fileUpload.resolve(tempFiles);
    });

    function queueTaskTest(fn) {
        return function() {
            var $fixture = $('#qunit-fixture');
            var instance = new (IssueDropZone.extend({
                $node: $fixture,
                init: this.sandbox.stub(),
                pendingQueue: [],
                markDirty: this.sandbox.stub()
            }));

            fn($fixture, instance, IssueDropZone, $);
        }
    }

    test("IssueDropZone queueTask single test", queueTaskTest(function($fixture, instance, IssueDropZone, $) {
        var task = new $.Deferred();
        instance.queueTask(task);
        ok(instance.markDirty.calledWith(true), "markDirty called with true");

        task.resolve();
        ok(instance.markDirty.calledWith(false), "markDirty called with false");
    }));

    test("IssueDropZone queueTask multiple test", queueTaskTest(function($fixture, instance, IssueDropZone, $) {
        var task1 = new $.Deferred(), task2 = new $.Deferred();

        instance.queueTask(task1);
        ok(instance.markDirty.calledWith(true), "markDirty called with true");

        instance.queueTask(task2);
        ok(instance.markDirty.calledWith(true), "markDirty called with true");

        task1.resolve();
        ok(!instance.markDirty.calledWith(false), "markDirty not called with false");

        task2.resolve();
        ok(instance.markDirty.calledWith(false), "markDirty called with false");
    }));
});
