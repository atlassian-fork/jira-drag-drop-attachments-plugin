AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources"], function () {
    "use strict";

    var path = require("dndattachment/ctrlv/path");

    module("dndattachment/ctrlv/basename");

    test("should return only the last path component of the URI", function (assert) {
        assert.equal(path.basename(""), "");
        assert.equal(path.basename("/"), "");
        assert.equal(path.basename("/a"), "a");
        assert.equal(path.basename("/a/"), "");
        assert.equal(path.basename("/a/b"), "b");
    });

    module("dndattachment/ctrlv/dirname");

    test("should return everything leading up to the basename", function (assert) {
        assert.equal(path.dirname(""), "");
        assert.equal(path.dirname("/"), "/");
        assert.equal(path.dirname("/a"), "/");
        assert.equal(path.dirname("/a/"), "/a/");
        assert.equal(path.dirname("/a/b"), "/a/");
    });
});
