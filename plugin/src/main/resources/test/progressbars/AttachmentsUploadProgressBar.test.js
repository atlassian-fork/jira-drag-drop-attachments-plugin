AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:dnd-issue-drop-zone"
], function() {
    var DnDTemplates = require('dndattachment/templates');
    var $ = require('jquery');

    module("TestIssueDropZone", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();

            this.context.mock('dndattachment/util/Configuration', {
                getWRM: function() { return '' },
            });
        },

        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("AttachmentsUploadProgressBar uploadFilenameXSS test", function() {
        var flagSpy = {
            showErrorMsg: sinon.spy(),
        };

        this.context.mock('jira/flag', flagSpy);

        var AttachmentsUploadProgressBar = this.context.require('dndattachment/progressbars/AttachmentsUploadProgressBar');

        var $fixture = $('#qunit-fixture');

        var $node = $fixture.html(DnDTemplates.UploadProgressBar({}));

        var instance = new (AttachmentsUploadProgressBar.extend({
            $node: $node,
            loadThumbnail: this.sandbox.stub(),
            reportError: this.sandbox.stub(),
            getUploadParams: this.sandbox.stub()
        }))($node);

        var messageXss = 'this is message <script>alert("xss")</script>';
        var messageExpected = 'this is message &lt;script&gt;alert(&quot;xss&quot;)&lt;/script&gt;';
        var titleExpected = 'dnd.attachment.not.uploaded';
        AttachmentsUploadProgressBar.prototype.showErrorMessage.call(instance, messageXss, '');

        ok(flagSpy.showErrorMsg.calledOnce);
        ok(flagSpy.showErrorMsg.calledWith(titleExpected, messageExpected), 'Message should be html escaped');
    });
});
