AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"
], function(){
    "use strict";

    var $ = require('jquery');
    var Parser = require('dndattachment/Parser');

    module("TestParser", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
        },

        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("Parser test", function(assert) {
        var done = assert.async();
        var $fixture = $('#qunit-fixture');

        ok(Parser.parse, 'Parser.parse is not null');
        ok(typeof Parser.parse == "function", 'Parser.parse is function');

        $('<span duiType="TestDuiType"> </span>').appendTo($fixture);

        var TestDuiTypeConstructor = this.sandbox.stub();
        define('TestDuiType', function() {
            return TestDuiTypeConstructor;
        });

        $.when.apply(window, [Parser.parse($fixture), Parser.parse($fixture)]).then(function() {
            ok(TestDuiTypeConstructor.calledOnce, 'TestDuiTypeConstructor called once');
            done();
        });
    });
});

