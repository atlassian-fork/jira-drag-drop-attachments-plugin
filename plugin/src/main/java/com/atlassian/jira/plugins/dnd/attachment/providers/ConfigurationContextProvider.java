package com.atlassian.jira.plugins.dnd.attachment.providers;

import com.atlassian.core.util.FileSize;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.security.random.DefaultSecureTokenGenerator;
import com.atlassian.security.random.SecureTokenGenerator;
import com.google.common.collect.ImmutableMap;
import webwork.config.Configuration;

import java.util.Map;

public class ConfigurationContextProvider extends AbstractJiraContextProvider
{
    private SecureTokenGenerator tokenGenerator = DefaultSecureTokenGenerator.getInstance();
    @Override
    public Map getContextMap(ApplicationUser user, JiraHelper jiraHelper)
    {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        // because it doesn't work via ApplicationProperties on OnDemand :(
        String attachmentSize = Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE);
        builder.put("jiraAttachmentSize", attachmentSize);
        builder.put("uploadLimit", FileSize.format(new Long(attachmentSize)));
        builder.put("formToken", tokenGenerator.generateToken());
        return builder.build();
    }
}
