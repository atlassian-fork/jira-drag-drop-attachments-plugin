package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import org.openqa.selenium.By;

public class IssueCreatedMessage extends GlobalMessage {

    public String getIssueKey() {
        return this.message.find(By.cssSelector(".issue-created-key")).getAttribute("data-issue-key");
    }

}
