package it.com.atlassian.plugins.jira.screenshot.pageobjects;

import com.atlassian.jira.pageobjects.dialogs.FormDialog;
import org.openqa.selenium.By;

public class AttachScreenshotDialog extends FormDialog
{
    public AttachScreenshotDialog()
    {
        super("attach-screenshot-dialog");
    }

    public boolean isEmpty() {
        return !this.find(By.cssSelector(".attach-screenshot-pasted-image")).isPresent();
    }

    public String getFileName() {
        return this.find(By.id("attachscreenshotname")).getValue();
    }

    public void submit()
    {
        this.submit(By.id("attach-screenshot-html5-upload"));
    }

    public void setFileName(final String fileName)
    {
        this.find(By.id("attachscreenshotname")).clear().type(fileName);
    }
}
