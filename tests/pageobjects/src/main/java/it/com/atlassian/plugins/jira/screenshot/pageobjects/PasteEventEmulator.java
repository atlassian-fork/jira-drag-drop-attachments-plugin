package it.com.atlassian.plugins.jira.screenshot.pageobjects;

import com.atlassian.jira.pageobjects.util.UserSessionHelper;
import com.atlassian.pageobjects.elements.PageElement;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class PasteEventEmulator {

    public static void emulatePaste(PageElement element, String fileName) {
        element.javascript().execute(readResource("js/paste-emulator.js"), fileName);
    }

    public static void emulatePasteMultiContentRtfAndImage(PageElement element, String fileName) {
        element.javascript().execute(readResource("js/multi-type-paste-emulator.js"), fileName);
    }

    private static String readResource(String name)
    {
        final ClassLoader loader = UserSessionHelper.class.getClassLoader();
        final InputStream resourceAsStream = loader.getResourceAsStream(name);
        try
        {
            return IOUtils.toString(resourceAsStream, "utf-8");
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            IOUtils.closeQuietly(resourceAsStream);
        }
    }
}
