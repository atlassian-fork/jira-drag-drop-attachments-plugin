package it.com.atlassian.plugins.jira.screenshot.webdriver;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.config.DisableRTE;
import com.atlassian.jira.pageobjects.pages.viewissue.AddCommentSection;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import it.com.atlassian.plugins.jira.screenshot.pageobjects.AttachScreenshotDialog;
import it.com.atlassian.plugins.jira.screenshot.pageobjects.ViewIssuePage;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.regex.Pattern;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static org.apache.commons.lang3.StringUtils.trim;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest({ WEBDRIVER_TEST })
@Category({ OnDemandAcceptanceTest.class })
public class TestIssuePaste extends BasePasteWebTest
{
    @Test
    public void testViewIssuePaste()
    {
        ViewIssuePage issuePage = pageBinder.navigateToAndBind(ViewIssuePage.class, issueKey);

        int originalAttachmentCount = getAttachmentCount();

        issuePage.pasteImage(FILE_NAME);

        AttachScreenshotDialog screenshotDialog = pageBinder.bind(AttachScreenshotDialog.class);

        assertTrue("Dialog is visible", screenshotDialog.isOpen().byDefaultTimeout());
        assertFalse("Dialog is not empty", screenshotDialog.isEmpty());
        assertEquals("Screenshot file name is preset", "screenshot-1", screenshotDialog.getFileName());

        screenshotDialog.submit();

        int newAttachmentCount = getAttachmentCount();

        assertTrue("Attachment count increased by one", newAttachmentCount - originalAttachmentCount == 1);
    }

    @Test
    public void testPastingMultiTypeContentWithRtfAndImagePastesText()
    {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "comment", "Wiki Style Renderer");
        ViewIssuePage issuePage = pageBinder.navigateToAndBind(ViewIssuePage.class, issueKey);
        issuePage.pasteMultiContentRtfAndImage(issuePage.getCommentField(), FILE_NAME);
    }

    @Test
    @DisableRTE
    public void testCommentFieldPaste()
    {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "comment", "Wiki Style Renderer");

        ViewIssuePage issuePage = pageBinder.navigateToAndBind(ViewIssuePage.class, issueKey);

        AddCommentSection commentSection = issuePage.comment();

        issuePage.pasteImage(issuePage.getCommentField(), FILE_NAME);

        Tracer tracer = traceContext.checkpoint();

        AttachScreenshotDialog screenshotDialog = pageBinder.bind(AttachScreenshotDialog.class);

        final String fileName = generateFileName();
        screenshotDialog.setFileName(fileName);
        screenshotDialog.submit();

        traceContext.waitFor(tracer, "AJS.$.ajaxComplete", Pattern.compile(jira.getProductInstance().getContextPath() + "/secure/AjaxIssueAction!default.jspa"));

        assertEquals("Screenshot markup inserted to comment field", String.format("!%s.png|thumbnail!", fileName), commentSection.getInput().trim());
    }

    private String generateFileName() {return "image-" + Math.floor(Math.random() * 100);}

    @Test
    public void testCommentFieldPasteOnTextRendererEnabled()
    {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "comment", "Default Text Renderer");
        ViewIssuePage issuePage = pageBinder.navigateToAndBind(ViewIssuePage.class, issueKey);

        Tracer tracer = traceContext.checkpoint();
        issuePage.pasteImage(issuePage.getCommentField(), FILE_NAME, false);

        traceContext.waitFor(tracer, "jira/attach-images-plugin/pasteIgnored");
    }

    @Test
    @DisableRTE
    public void testDescriptionFieldPaste()
    {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "description", "Wiki Style Renderer");

        ViewIssuePage issuePage = pageBinder.navigateToAndBind(ViewIssuePage.class, issueKey);

        Tracer tracer = traceContext.checkpoint();

        PageElement descriptionEdit = issuePage.goToDescriptionEdit();
        issuePage.pasteImage(descriptionEdit, FILE_NAME);

        AttachScreenshotDialog screenshotDialog = pageBinder.bind(AttachScreenshotDialog.class);
        final String fileName = generateFileName();
        screenshotDialog.setFileName(fileName);
        screenshotDialog.submit();

        traceContext.waitFor(tracer, "AJS.$.ajaxComplete", Pattern.compile(jira.getProductInstance().getContextPath() + "/secure/AjaxIssueAction!default.jspa"));

        assertEquals("Screenshot markup inserted to comment field", String.format("!%s.png|thumbnail!", fileName), trim(descriptionEdit.getValue()));

        assertTrue("Description edit field is still visible", descriptionEdit.isVisible());
    }

    @Test
    public void testDisableIssuePasteViaDarkFeature()
    {
        backdoor.fieldConfiguration().setFieldRenderer("Default Field Configuration", "comment", "Wiki Style Renderer");
        ViewIssuePage issuePage = pageBinder.navigateToAndBind(ViewIssuePage.class, issueKey);
        issuePage.pasteImage(issuePage.getCommentField(), FILE_NAME, false);
        AttachScreenshotDialog screenshotDialog = pageBinder.bind(AttachScreenshotDialog.class);
        assertTrue("Screenshot dialog is open", screenshotDialog.isOpen().byDefaultTimeout());
    }
}
