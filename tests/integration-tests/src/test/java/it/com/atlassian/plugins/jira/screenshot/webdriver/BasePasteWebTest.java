package it.com.atlassian.plugins.jira.screenshot.webdriver;

import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;

import javax.inject.Inject;
import java.util.Collection;

import static com.atlassian.jira.functest.framework.FunctTestConstants.DEFAULT_FIELD_CONFIGURATION;
import static com.atlassian.jira.functest.framework.FunctTestConstants.FIELD_COMMENT;
import static com.atlassian.jira.functest.framework.FunctTestConstants.WIKI_STYLE_RENDERER;
import static it.com.atlassian.AuiUtils.closeAllAuiFlags;

public abstract class BasePasteWebTest extends BaseJiraWebTest
{
    static final String FILE_NAME = "image.png";

    private static final String ISSUE_SUMMARY = "DnD FTW";
    private static final String PROJECT_KEY = "TP";
    private static final String PROJECT_NAME = "Test project";
    private static final String USERNAME = "admin";

    static volatile String issueKey;
    
    private static volatile IssueTypeControl.IssueType primaryIssueType;

    @Inject
    TraceContext traceContext;

    @Inject
    PageElementFinder finder;

    @Rule
    public JavaScriptErrorsRule javaScriptErrorsRule = new JavaScriptErrorsRule();

    @BeforeClass
    public static void setUpClass()
    {
        backdoor.project().addProject(PROJECT_NAME, PROJECT_KEY, USERNAME);
        primaryIssueType = backdoor.issueType().createIssueType("Temporary primary issue type");
    }

    @AfterClass
    public static void tearDownClass()
    {
        backdoor.issueType().deleteIssueType(Long.parseLong(primaryIssueType.getId()));
        backdoor.project().deleteProject(PROJECT_KEY);
        backdoor.fieldConfiguration().setFieldRenderer(DEFAULT_FIELD_CONFIGURATION, FIELD_COMMENT, WIKI_STYLE_RENDERER);
        backdoor.fieldConfiguration().setFieldRenderer(DEFAULT_FIELD_CONFIGURATION, "description", WIKI_STYLE_RENDERER);
    }

    @Before
    public void setUp()
    {
        issueKey = backdoor.issues().createIssue(PROJECT_KEY, ISSUE_SUMMARY).key();
        jira.quickLoginAsAdmin();
        jira.gotoHomePage();
        closeAllAuiFlags(jira);
    }

    @After
    public void tearDown()
    {
        backdoor.issues().deleteIssue(issueKey, true);
    }

    int getAttachmentCount()
    {
        return ((Collection<?>) backdoor.issues().getIssue(issueKey).fields.get("attachment")).size();
    }
}
